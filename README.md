# Fluid-Slurm-GCP Spack #
Copyright 2020 Fluid Numerics LLC
This repository contains spack configuration files for fluid-slurm-gcp HPC cluster solutions.

Copy the contents of `etc-spack` to `/etc/spack` (as root) on your fluid-slurm-gcp deployment to obtain
useful spack site configurations for your fluid-slurm-gcp HPC cluster.
